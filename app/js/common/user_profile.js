userProfileApp = angular.module("user.profile", [ "ngResource" ]);

userProfileApp.factory('userService', [ '$resource', userService ]);
userProfileApp.factory('userProfile', [ 'userService', userProfile ]);

function userService ( $resource ) {
  return $resource( '/SRA/:action', null , {
      login: { method: 'POST' },
      logout: { method: 'POST' }
  });
};
  
function userProfile ( userService ) {
  var _sessionId = undefined;
  var _userName = undefined;
  var _firstName = undefined;
  var _lastName = undefined;

  var _randomString = function (length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
  };

  var _login =  function(user, pass, externalSuccessHandler, externalErrorHandler ) {
      var token = _randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
      var _successHandler = function( response ) {
          _sessionId = response.sessionId;
          _userName = response.data.username;
          _firstName = response.data.firstname;
          _lastName = response.data.lastname;
          if ( angular.isFunction( externalSuccessHandler ) ) { externalSuccessHandler.call( response ) }; 
      };
      
      userService.login( { action: 'authenticate' }, {
          "token": token,
          "digest": btoa(md5(user + ',' + btoa(md5(pass) + ',' + token ))), 
          "user": {
            "username": user,
            "password": md5(pass)
          }
      }, _successHandler, externalErrorHandler);

  };
  
  var _logout =  function( externalSuccessHandler, externalErrorHandler ) {
    var _successHandler = function ( response ) {
        _sessionId = undefined;
        _userName = undefined;
        _firstName = undefined;
        _lastName = undefined;
        if ( angular.isFunction( externalSuccessHandler ) ) { externalSuccessHandler.call( response ) }; 
    };
    userService.logout({ action: 'logout'}, { sessionId: _sessionId }, _successHandler, externalErrorHandler);
  };
  
  var _isLoggedIn =  function() {
      return (_sessionId !== undefined); 
  };
  
  var _getSessionId = function () {
    return _sessionId;
  };

  return {
    login: _login,
    logout: _logout,
    isLoggedIn: _isLoggedIn,
    getSessionId: _getSessionId
  };
};

