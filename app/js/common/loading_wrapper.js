loadingWrapperApp = angular.module("loadingWrapper.app", []);

loadingWrapperApp.directive("loadingWrapper", loadingWrapperDirective);

function loadingWrapperDirective() {
  return {
    restrict: 'A',
    scope: {
        loadingWrapper: "="
    },
    link: function($scope, elem, attr ){
        $scope.$on(attr.loadingWrapper, function( event, isLoading ) {
            if (typeof isLoading !== "undefined" && isLoading !== null)  {
              if (isLoading === true ) {
                var wrapper = angular.element('<div>');
                wrapper.addClass('loading');
                $(elem).prepend(wrapper);
              } else {
                angular.element($(elem)[0]).find('.loading').remove();
              };
            }
        })
    }
  }
}