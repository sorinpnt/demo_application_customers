app = angular.module("app", [
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'loadingWrapper.app',
    'user.profile',
    'login.app',
    'home.app',
    'customer.app'
]);

app.config(['$stateProvider', '$urlRouterProvider', routerConfig ]);
app.controller('appCtrl', appController );
app.run(setupApp);

function routerConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise('login');
  $stateProvider.state('login', {
    url: '/login',
    templateUrl: 'app/routes/login/login.html',
    controller: 'loginCtrl as login'
  });
  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'app/routes/home/home.html',
    controller: 'homeCtrl as home'
  });
  $stateProvider.state('customer', {
    url: '/customer/{customerId:int}',
    templateUrl: 'app/routes/customer/customer.html',
    controller: 'customerCtrl as cstm'
  });
};

function appController( userProfile, $state ) {
  this.logout = function() {
    var _logoutSuccess = function() { $state.go('login');};
    var _logoutError = function() {};
    userProfile.logout( _logoutSuccess, _logoutError );
  };
};

function setupApp($state, $rootScope, $http, userProfile) {
    $rootScope.pageTitle = "Sales Application";
    $rootScope.$state = $state;
    var checkLoginBeforeStateChange = function(event, toState, toParams, fromState, fromParams) {
      if ( (fromState.name === '' && toState.name !== 'login') || toState.name !== 'login') {
        if ( userProfile.isLoggedIn() === false ) {
          event.preventDefault();
          $state.go('login');
        }
      };
    };
    $rootScope.$on ( '$stateChangeStart', checkLoginBeforeStateChange );
};


app.filter('orderByObjectProperty', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }

    array.sort(function(a, b){
        a = parseInt(a[attribute]);
        b = parseInt(b[attribute]);
        return a - b;
    });
    return array;
 }
});