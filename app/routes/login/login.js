loginApp = angular.module("login.app", []);

loginApp.controller( 'loginCtrl', loginController );

function loginController( $rootScope, $scope, userProfile, $state ){
    var loginSuccess = function( data ) {
        $rootScope.$broadcast('login_section_loader', false);
        $state.go('home');
    }
    var loginErr = function( data ) { 
        $rootScope.$broadcast('login_section_loader', false);
    }
    this.submit = function() {
        $rootScope.$broadcast('login_section_loader', true);
        userProfile.login( $scope.user, $scope.pass, loginSuccess, loginErr )
    };
};
