customerApp = angular.module("customer.app", []);

customerApp.controller( 'customerCtrl', customerController );
customerApp.service( 'customerService', customerService );

function customerService ( $resource ) {
  return $resource( 'SRA/customer/:id', null, {
      getCustomer: { method: 'POST' },
      saveCustomer: {method: 'PUT'}
  });
};

function customerController( $rootScope, $stateParams, $scope, $filter, userProfile, customerService) {
   var _customerSuccess = function( response ) {
        $scope.customer.visit.date = $filter('date')(Date(), 'DD-MM-YYYY');
        $scope.customer.visit.time = $filter('date')(Date(), 'HH:mm');
        console.log ( $scope.customer.visit.date + ' ' + $scope.customer.visit.time);
        $scope.customer = response.data[0];
        $rootScope.pageTitle = $scope.customer.customername;
   };
   var _customerError = function( response ) {};

   customerService.getCustomer({ id: $stateParams.customerId },
    { sessionId: userProfile.getSessionId() }, _customerSuccess, _customerError);

   this.saveChanges = function() {
        var customerDetails = $scope.customer;
        var _saveSuccess = function ( response ) {
            alert(response.reason);
        };
        var _saveError = function ( response ) {};
        customerService.saveCustomer( {id: $scope.customer.id}, {
            sessionId: userProfile.getSessionId(),
            customerDetails: customerDetails
        }, _saveSuccess, _saveError );
   };

   activateBootstrapTabs();
};

function activateBootstrapTabs() {
  $('#myTabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  })
};