homeApp = angular.module("home.app", []);

homeApp.controller( 'homeCtrl', homeController );
homeApp.factory( 'homeService', homeService );

function homeService ( $resource ) {
  return $resource( 'SRA/customer/list', null, {
      getList: { method: 'POST' }
  });
};

function homeController( $rootScope, userProfile, homeService, $scope, $filter ){
    var _= this;
    _.lastSortingKey = 'id';
    _.lastSortingOrder = true;
    _.unfilteredCustomerList = undefined;
    $rootScope.pageTitle = "Sales App";

    _.filterList = function() {
        $rootScope.$broadcast('home_state_loader', true);
        var _filterFunction = function ( customerObj ) {
            if ( customerObj[$scope.filterKey] == $scope.filterWord ) {
                return customerObj;
            }
        };
        $scope.customerList = $filter('filter')( _.unfilteredCustomerList, _filterFunction );
        $rootScope.$broadcast('home_state_loader', false);
    };

    _.resetFilter = function() {
        $scope.customerList = _.unfilteredCustomerList;
        $scope.filterKey = 'id';
        $scope.filterWord = '';
    };

    _.sortList = function( sortingKey, ascendingOrder ) {
        $rootScope.$broadcast('home_state_loader', true);
        if ( sortingKey === undefined) {
            sortingKey = _.lastSortingKey;
        } else {
            _.lastSortingKey = sortingKey;
        }
        if ( ascendingOrder === undefined ) {
            ascendingOrder = _.lastSortingOrder;
        } else {
            _.lastSortingOrder = ascendingOrder;
        }
        $scope.customerList = $filter('orderBy')($scope.customerList, sortingKey, ascendingOrder );
        $rootScope.$broadcast('home_state_loader', false);
    };

    _.loadCustomerList = function() {
        $rootScope.$broadcast('home_state_loader', true);

        var _success = function ( response ) {
            _.unfilteredCustomerList = response.data;
            $scope.customerList = _.unfilteredCustomerList;
            $rootScope.$broadcast('home_state_loader', false);
        };

        var _error = function ( response ) {
            $rootScope.$broadcast('home_state_loader', false);
        };
        homeService.getList({ sessionId: userProfile.getSessionId() }, _success, _error);
   };
   _.loadCustomerList();
};