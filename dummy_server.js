var express = require('express');
var bodyParser = require('body-parser');
var server = express();

var SERVER_PORT = 10001;
var SESSION_ID = "75984292-af89-4280-8aac-7f15b4e3a1ba";
var sra_authenticate_200_response = {
    code: 0,
    message: "Login Successful",
    sessionId: "75984292-af89-4280-8aac-7f15b4e3a1ba",
    data:{
        username: "john.doe",
        firstname: "John",
        lastname: "Doe"
    }
};
var sra_authenticate_404_response = {
    code: 1,
    message: "Error in login data for user with name john",
    sessionId: null,
    data: null
};
var sra_logout_200_response =  {
    code:0,
    message: "Logout Successful",
    sessionId:null,
    data:null
};
var sra_logout_501_response = {
    reason: "Not implemented"
};
var sra_customer_list_200_response = {
    code: 0,
    message: "Successfully",
    sessionId: "75984292-af89-4280-8aac-7f15b4e3a1ba", 
    data: [{
        id: 42,
        customername :"Amul",
        productname: "Sorting Machine",
        status: "New",
        email: 'amul@sm.com',
        notes: "Testing",
        phonenumber: '02141512412',
        username: "john.doe",
        notes: {
            text: 'Random note',
            status:{ 
                'New': true,
                'InProgress': false,
                'OrderPlaced': true,
                'Support': true,
                'Cancelled': false,
                'Rejected': false
            }
        }
    },
    {
        id:41,
        customername: "Biltmore Global Superstore",
        productname: "Color Swatch",
        status: "InProgress",
        notes: "Auth Testing",
        username: "john.doe",
        email: 'Biltmore@cs.com',
        phonenumber: '3222352323',
        visit: {
            date: '15-06-2016',
            time: '17:40',
            status: {
                "Offer": false,
                "NewCustomer": true,
                "Lead": true,
                "Oportunity": false
            },
            notes: 'Bring cookies'
        },
        notes: null
    },
    {
        id: 40,
        customername: "Arrow",
        productname: "Aimpoint 9000SC",
        status: "OrderPlaced",
        notes: null,
        username: "john.doe",
        email: 'Arrow@op.com',
        phonenumber: '743543'
    },
    {
        id: 39,
        customername: "Promotional Shop",
        productname: "Aimpoint CompC3",
        status: "Support",
        notes: null,
        username: "john.doe",
        email: 'promotional@supp.com',
        phonenumber: '0245712'
    },
    {
        id: 38,
        customername: "The Smooth Mover",
        productname: "Barska 6-24x50",
        status: "Rejected",
        notes: null,
        username: "john.doe",
        email: 'smoothMover@bsk.com',
        phonenumber: '02141512'
    },
    {
        id: 37,
        customername: "Karioi Clinic",
        productname: "BSA 3-9x50",
        status: "New",
        notes: null,
        username: "john.doe",
        email: 'Karioi@bsa.com',
        phonenumber: '0158625412'
    },
    {
        id: 36,
        customername: "Property Net",
        productname: "Fullfield DAC",
        status: "InProgress",
        notes: null,
        username: "john.doe",
        email: 'pnet@full.com',
        phonenumber: '0212412'
    }]
};
var sra_customer_list_403_response = {
    reason: 'Authentication required'
};
var sra_customer_customerId_200_response = {
    code: 0,
    message: "Successfully",
    sessionId: "75984292-af89-4280-8aac-7f15b4e3a1ba", 
    data: []
};
var sra_customer_customerId_404_response = {
    reason: 'No customer found'
}

server.use(express.static(__dirname));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.post('/SRA/authenticate', function( req, res ){
    var user = req.body.user.username;
    var md5pass =  req.body.user.password;
    if ( user === 'john.doe' && md5pass === '21232f297a57a5a743894a0e4a801fc3' ) {
        res.status(200).send(sra_authenticate_200_response);
    }
    res.status(404).send(sra_authenticate_404_response);
});

server.post('/SRA/logout', function( req, res ){
    var session_id = req.body.sessionId;
    if ( session_id === SESSION_ID ) {
        res.status(200).send(sra_logout_200_response);
    }
    res.status(501).send(sra_logout_501_response);
});

server.post('/SRA/customer/list', function(req, res ) {
    var session_id = req.body.sessionId;
    if ( session_id === SESSION_ID ) {
        res.status(200).send(sra_customer_list_200_response);
    }
    res.status(403).send(sra_customer_list_403_response);
});

server.post('/SRA/customer/:customerId', function(req, res ) {
    sra_customer_customerId_200_response.data = [];
    var session_id = req.body.sessionId;
    var customerId = req.params.customerId;
    if ( session_id === SESSION_ID ) {
        var tempCst = undefined;
        sra_customer_list_200_response.data.forEach( function( customer ) {
        if ( customer.id ==customerId &&  sra_customer_customerId_200_response.data.length === 0 ) {
            tempCst = customer;
            }
        });
        sra_customer_customerId_200_response.data[0] = tempCst;
        if (  sra_customer_customerId_200_response.data.length === 0 ) {
            res.status(404).send(sra_customer_customerId_404_response);
        }
        res.status(200).send(sra_customer_customerId_200_response);
    }
    res.status(403).send(sra_customer_list_403_response);
});

server.put('/SRA/customer/:customerId', function(req, res ) {
    var session_id = req.body.sessionId;
    var customerDetails = req.body.customerDetails;
    if ( session_id === SESSION_ID ) {
        sra_customer_list_200_response.data.forEach( function( customer, customerIndex ) {
        if ( customer.id == customerDetails.id ) {
            sra_customer_list_200_response[customerIndex] = customerDetails;
            res.status(200).send({reason: 'Customer updated uccessfully'});
            }
        });
            res.status(404).send(sra_customer_customerId_404_response);
    }
    res.status(403).send(sra_customer_list_403_response);
});

server.listen(SERVER_PORT, function() {
    console.log('server listening on port ' + SERVER_PORT);
});